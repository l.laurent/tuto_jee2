package payroll;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
class LoadDatabase {

	@Bean
	CommandLineRunner initDatabase(EmployeeRepository repository) {
		return args -> {
			log.info("Preloading " + repository.save(new Employee("Frederic",  "Mollas", "Chef")));
			log.info("Preloading " + repository.save(new Employee("Violette", "Lalisse", "DG")));
			log.info("Preloading " + repository.save(new Employee("Sebastien", "Rassiat", "Responsable marketing")));
			OrderRepository.save(new Order("MacBook Pro", Status.COMPLETED));
			OrderRepository.save(new Order("iPhone", Status.IN_PROGRESS));

			OrderRepository.findAll().forEach(order -> {
				log.info("Preloaded " + order);
			});
		};
	}
}