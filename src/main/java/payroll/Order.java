package payroll;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Data
@Table(name = "CUSTOMER_ORDER")
class Order {

	private @Id @GeneratedValue Long id;

	private String description;
	private payroll.Status status;

	Order(String description, payroll.Status completed) {

		this.description = description;
		this.status = completed;
	}
}